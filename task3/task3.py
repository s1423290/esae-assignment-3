#!/bin/python3
from pprint import pprint
import csv, json
import enum

import hashlib
from Crypto.Util.asn1 import DerSequence
from Crypto.PublicKey import RSA
from binascii import a2b_base64


# Enums to distinguish between different TLSA options
CertificateUsage = {
	'CA_CERTIFICATE': 0,
	'END_ENTITY': 1, 
	'TRUST_ANCHOR': 2,
	'DOMAIN_ISSUED': 3, 
}

Selector = {
	'FULL_CERTIFICATE': 0, 
	'SUBJECTPUBLICKEYINFO': 1 
}

MatchingType = {
	'EXACT': 0, #
	'SHA256': 1, #
	'SHA512': 2 #
}

### Compares single TLSA record to server certificates (certificate+chain) and check whether they are consistent
def verify_certificates(tlsa_record, server_certificates):

	### Find correct Certificate to use - CertificateUsage
	certificates = [];
	# CA certificate must be one of the chain certificates
	if tlsa_record['certificate_usage'] == CertificateUsage['CA_CERTIFICATE']:
		# no chain means verification has failed
		if 'chain' not in server_certificates or len(server_certificates['chain']) == 0:
			return False

		certificates = server_certificates['chain'] 
		
	# Certificate used by end entity (this option requires to validate the certificate chain, but the assignment ignores this requirement)
	elif tlsa_record['certificate_usage'] == CertificateUsage['END_ENTITY']:
		certificates = [server_certificates['certificate']]

	# trust anchor certificate must be one of the chain certificates
	elif tlsa_record['certificate_usage'] == CertificateUsage['TRUST_ANCHOR']:
		# no chain means verification has failed
		if 'chain' not in server_certificates or len(server_certificates['chain']) == 0:
			return False

		certificates = server_certificates['chain'] 
	# Certificate used by end entity (without validating chain path)
	elif tlsa_record['certificate_usage'] == CertificateUsage['DOMAIN_ISSUED']:
		certificates = [server_certificates['certificate']]

	# invalid TLSA - should never happen
	else:
		return False
	


	### Find property of certificate to match to hash - Selector
	cmp_values = ['']*len(certificates)
	for i in range(0, len(certificates)):
		certificate = certificates[i]
		# Compare the full (DER) certificate
		if tlsa_record['selector'] == Selector['FULL_CERTIFICATE']: 
			cmp_values[i] = a2b_base64(certificate['raw'].replace(" ",''))

		# Compare the subjectpublickeyinfo from certificate
		elif tlsa_record['selector'] == Selector['SUBJECTPUBLICKEYINFO']: 
			der = a2b_base64(certificate['raw'].replace(" ",''))
			cert = DerSequence()
			cert.decode(der)
			tbsCertificate = DerSequence()
			tbsCertificate.decode(cert[0])
			cmp_values[i] = tbsCertificate[6]

	
	### Match property value to hash(or raw), return comparison results - MatchingType
	for cmp_value in cmp_values:
		if tlsa_record['matching_type'] == MatchingType['EXACT']:
			if tlsa_record['hash'] == cmp_value.hex():
				return True
		elif tlsa_record['matching_type'] == MatchingType['SHA256']:
			if tlsa_record['hash'] == hashlib.sha256(cmp_value).hexdigest():
				return True
		elif tlsa_record['matching_type'] == MatchingType['SHA512']:
			if tlsa_record['hash'] == hashlib.sha512(cmp_value).hexdigest():
				return True

	# No successful match
	return False



### Read and store TLSA records for scanned domain
tlsa_records_reader = csv.reader(open('tranco.tlsa_domain', 'r'))
tlsa_records = dict()
# CSV format: certificate usage, selector, matching type, hash, domain
for data in tlsa_records_reader:

	# remove _443._tcp. DNS prefix to get domain name
	domain = data[4]
	if domain.startswith('_443._tcp.'):
		domain = domain[10:]

	# support multiple TLSA records
	if domain not in tlsa_records:
		tlsa_records[domain] = list()

	tlsa_records[domain].append({
		'certificate_usage': int(data[0]),
		'selector': int(data[1]),
		'matching_type': int(data[2]),
		'hash': data[3]
	})



### read through tranco zgrab scan to find domains matching TLSA records, as well as count the number of domains in the tranco list
tranco_domains = set()
json_reader = open('tranco.zgrab', 'r')
consistent_domains = dict() # result IP=>BOOLEAN
for data in json_reader:
	data = json.loads(data)
	
	# store statistics about domains in tranco list
	tranco_domains.add(data['domain'])

	# filter zgrab scan results to find one that matches the TLSA records
	if data['domain'] not in tlsa_records:
		continue;
	if data['data']['tls']['status'] != 'success':
		continue

	# domain's certificates has already matched the TLSA record, no need to verify again
	if data['domain'] in consistent_domains and consistent_domains[data['domain']] == True:
		continue

	# verify whether TLSA records are consistent with the server certificates from zgrab scan
	consistent_domains[data['domain']] = False
	for tlsa_record in tlsa_records[data['domain']]:
		if verify_certificates(tlsa_record, data['data']['tls']['result']['handshake_log']['server_certificates']):
			consistent_domains[data['domain']] = True
			break

	# pprint(str(n) + " - " + data['domain'])


### Display results
pprint(consistent_domains)
pprint("number of domains: " + str(len(tranco_domains)))
pprint("Number of domains with a TLSA record: " + str(len(tlsa_records)) + "=" + str(round(len(tlsa_records)/len(tranco_domains)*100, 1)) + "% of domains")

# count consistent domains
n = 0
for domain in consistent_domains:
	if consistent_domains[domain] == True:
		n += 1

pprint("Number of domains with a TLSA record consistent with certificate: " + str(n))




