#!/bin/python3

# regular imports
import copy
import json
import pprint

# avro parsing imports
import avro
from avro.datafile import DataFileWriter, DataFileReader
from avro.io import DatumWriter, DatumReader

# domain name parsing import
from publicsuffix2 import PublicSuffixList

# read content from com-net-org
f = open('openintel.com-net-org.avro', 'rb') 
reader = DataFileReader(f, DatumReader())

# read content from other TLDs
f2 = open('openintel.czds.avro', 'rb')
reader2 = DataFileReader(f2, DatumReader())


# result variables containing filtered data from f/f2:
domain_results = dict()
nameserver_results = dict()

# init PublicSuffixList
psl = PublicSuffixList()

# max iterations (test code)
max_iter = -1 # value < -1 means ignore

##################### Read avro results and store relevant information in variables

# function definition to handle data from DNS request (read from reader/reader2)
def handleDNSRequest(data):
	# save domain name in result list
	if data['query_name'] not in domain_results:
		domain_results[data['query_name']] = False

	# Has a record been signed?
	if data['response_type'] == 'RRSIG': 
		domain_results[data['query_name']] = True

	# nameserver results?
	# response_type & request_type == NS: ns_address set
	if data['query_type'] == 'NS' and data['response_type'] == 'NS':
		# use TLD from domain queried instead of nameserver domain for DNSHOST1/2
		tld = psl.get_tld(data['query_name']) 
		ns_domain = psl.get_sld(data['ns_address'])

		if tld not in nameserver_results:
			nameserver_results[tld] = dict()

		if ns_domain not in nameserver_results[tld]:
			nameserver_results[tld][ns_domain] = set()

		if data['query_name'] not in nameserver_results[tld][ns_domain]:
			nameserver_results[tld][ns_domain].add(data['query_name'])

# loop through results com-net-org
iter = max_iter
for data in reader:
	# max iter?
	if iter == 0:
		break
	iter -= 1
	handleDNSRequest(data)
reader.close()

# loop through results other TLD's
iter = max_iter
for data in reader2:
	# max iter?
	if iter == 0:
		break
	iter -= 1
	handleDNSRequest(data)
reader2.close()
	
pprint.pprint(domain_results)
pprint.pprint(nameserver_results)


############################ analysis of results

# analysis of all results sorted per TLD
TLD_results = dict()
for domain in domain_results:
	# see if TLD already has a result variable/entry
	tld = psl.get_tld(domain)
	if tld not in TLD_results:
		TLD_results[tld] = dict({'domains': 0, 'secure_domains': 0, 'hosts': dict()})
	
	# count domains and secure domains
	TLD_results[tld]['domains'] += 1
	if domain_results[domain]:
		TLD_results[tld]['secure_domains'] += 1

# filter and collect TLD results - COMPTLD
TLD_results_COMPTLD = dict()
for tld in TLD_results:
	# assignment requirement, skip tld's with less than 100 domains
	if TLD_results[tld]['domains'] < 100: 
		continue
	# calculate percentage
	TLD_results_COMPTLD[tld] = round(TLD_results[tld]['secure_domains'] / TLD_results[tld]['domains'] * 100, 1)

# sort COMPTLD and write to CSV
TLD_results_COMPTLD = sorted(TLD_results_COMPTLD.items(), key=lambda x: x[1], reverse=True)
pprint.pprint(TLD_results_COMPTLD)
TLD_res_file = open('COMPTLD.csv', 'w')
for tld in TLD_results_COMPTLD:
	TLD_res_file.write(tld[0] + ',' + str(tld[1]) + "\n")
TLD_res_file.close()


# Analysis of all hosters per TLD - DNSHOST1/2
for tld in nameserver_results:
	
	# nameserver from different domain has been used.
	if tld not in TLD_results:
		continue

	for hoster in nameserver_results[tld]:

		# create records in TLD_results for hoster
		if hoster not in TLD_results[tld]['hosts']:
			TLD_results[tld]['hosts'][hoster] = {'domains': 0, 'secure_domains': 0}
		# count domains of hoster
		for domain in nameserver_results[tld][hoster]:
			TLD_results[tld]['hosts'][hoster]['domains'] += 1
			if domain_results[domain]:
				TLD_results[tld]['hosts'][hoster]['secure_domains'] += 1

	
# filter and output hoster data to CSV file per TLD - DNSHOST1/2
# this should be done for TLD's .net, and number 1,3 and 5 from TLD_results_COMPTLD
check_tlds = set()
if 'net' in TLD_results:
	check_tlds.add('net')

if len(TLD_results_COMPTLD) >= 1:
	check_tlds.add(TLD_results_COMPTLD[0][0])

if len(TLD_results_COMPTLD) >= 3:
	check_tlds.add(TLD_results_COMPTLD[2][0])

if len(TLD_results_COMPTLD) >= 5:
	check_tlds.add(TLD_results_COMPTLD[4][0])

pprint.pprint(check_tlds)

# collect hosts results for the selected TLDs
TLD_results_DNSHOST = dict()
for tld in check_tlds:
	# assignment requirement, skip tld's with less than 100 domains
	if TLD_results[tld]['domains'] < 100: 
		continue
	
	# sort top 10 hosts
	temp_res = sorted(TLD_results[tld]['hosts'].items(), key=lambda x: x[1]['domains'], reverse=True)
	
	# calculate output results for top 10 hosts
	TLD_results_DNSHOST[tld] = list()
	for i in range(0, min(10, len(temp_res))):
		TLD_results_DNSHOST[tld].append([temp_res[i][0], round(temp_res[i][1]['secure_domains']/temp_res[i][1]['domains']*100, 1)])
	TLD_results_DNSHOST[tld] = sorted(TLD_results_DNSHOST[tld], key=lambda x: x[1], reverse=True)

pprint.pprint(TLD_results_DNSHOST)

#output DNSHOST1/2 results
for tld in check_tlds:
	TLD_res_file = open('DNSHOST-' + tld +'.csv', 'w')
	for host in TLD_results_DNSHOST[tld]:
		TLD_res_file.write(host[0] + ',' + str(host[1]) + "\n")
	TLD_res_file.close()
