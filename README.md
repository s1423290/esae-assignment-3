# esae-assignment-3

## Required for execution of task 1:
``` sh
pip3 install avro
pip3 install python-snappy
pip3 install publicsuffix2
```

## required for execution of task3:
```sh
pip3 install pycrypto
```







## documentation sources:
AVRO: http://www.perfectlyrandom.org/2019/11/29/handling-avro-files-in-python/
public suffix: https://pypi.org/project/publicsuffix2/

