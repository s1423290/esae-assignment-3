#!/bin/bash

export PATH=/home/tls13/tls13/go/bin/:$PATH

export LC_ALL=C

INFILE=$1
INFILE_BASE=`basename $INFILE .gz`
OUTPUT_DIR=`dirname $INFILE`
USER=$2

TCPDUMP_OUTPUT_FILE=${OUTPUT_DIR}/zgrab.tcpdump.pcap
TCPDUMP_LOGFILE=${OUTPUT_DIR}/zgrab.tcpdump.log
ZGRAB_OUTPUT_FILE=${OUTPUT_DIR}/zgrab.json
ZGRAB_LOG_FILE=${OUTPUT_DIR}/zgrab.log

# Increase file limit
ulimit -Hn 1024000
ulimit -Sn 65535

# TCP: too many orphaned sockets
echo 16384 > /proc/sys/net/ipv4/tcp_max_orphans

# Adjust as needed
ZGRAB=/srv/mxtls/repos/github.com/zmap/zgrab2/cmd/zgrab2/zgrab2
TCPDUMP=/usr/sbin/tcpdump
TCPDUMP_IFACE=eno1

# shuffle input
zcat $INFILE | sed 's/:.*//g' > ${OUTPUT_DIR}/$INFILE_BASE.noport
cat ${OUTPUT_DIR}/$INFILE_BASE.noport | shuf > ${OUTPUT_DIR}/$INFILE_BASE.noport.shuf

# run tcpdump
$TCPDUMP -n -i $TCPDUMP_IFACE -w $TCPDUMP_OUTPUT_FILE "tcp port 853" 2> $TCPDUMP_LOGFILE &
TCPDUMPPID=$!
sleep 5

# run zgrab
$ZGRAB -o $ZGRAB_OUTPUT_FILE -f ${OUTPUT_DIR}/$INFILE_BASE.noport.shuf -l $ZGRAB_LOG_FILE tls -p 853

# stop tcpdump
/bin/kill -INT $TCPDUMPPID
sleep 5

gzip ${OUTPUT_DIR}/$INFILE_BASE.noport
gzip ${OUTPUT_DIR}/$INFILE_BASE.noport.shuf

chown -R $USER:$USER $OUTPUT_DIR
