#!/bin/bash

export PATH=/home/mxtls/mxtls/go/bin/:$PATH
export GOPATH=/home/mxtls/mxtls/experiment/lib/go/
export GOROOT=/home/mxtls/mxtls/go/

export LC_ALL=C

DATA_OUTPUT_DIR=$1
META_DIR=${DATA_OUTPUT_DIR}/meta
ZMAP_LOG_FILE=${META_DIR}/zmap.log
ZMAP_OUTPUT_FILE=${DATA_OUTPUT_DIR}/dot.zmap
OWNER=$2

mkdir -p $META_DIR

# Increase file limit
ulimit -Hn 1024000
ulimit -Sn 65535

# TCP: too many orphaned sockets
echo 16384 > /proc/sys/net/ipv4/tcp_max_orphans

ZMAP=/home/mxtls/mxtls/repos/github.com/zmap/zmap/src/zmap
ZMAP_CONF_FILE=/srv/mxtls/esae-week5-assignment-experiment/dot-scan/zmap.conf
BLACKLIST_DIR=/home/mxtls/mxtls/repos/gitlab.lrz.de/netintum/projects/gino/scan-meta

# copy configuration
cp $ZMAP_CONF_FILE $META_DIR

# run zmap
$ZMAP -N 10000 --config $ZMAP_CONF_FILE --blocklist-file ${BLACKLIST_DIR}/release/ipv4-bl-merged.txt --output-file $ZMAP_OUTPUT_FILE --log-file $ZMAP_LOG_FILE

gzip $ZMAP_OUTPUT_FILE

chown -R $OWNER:$OWNER $DATA_OUTPUT_DIR $META_DIR
