#!/bin/python3

# regular imports
import copy
import json
import pprint
import csv

# hold results of TLS handshake (zgrab result)
tls_results = dict()

# Loop through zgrab.json (zgrab results) to find out hosts that can establish a successful TLS connection, as these hosts are tested by dnslookup
# store zgrab result information in tls_results for successful TLS handshakes
json_reader = open('zgrab.json', 'r')
for data in json_reader:
	data = json.loads(data)
	if data['data']['tls']['status'] == 'success':
		tls_results[data['ip']] = data['data']['tls']['result']['handshake_log'];
		

# hold final results
true_dot_results = dict()

########################### verify whether the hosts with successful TLS connections are real DoT servers - TRUEDOT and CERT
# check the dnslookup results for each IP address
for ip in tls_results:
	# get dnslookup result for ip
	dnslookup_res = open('dnslookup-results/' + str(ip) + '.dnslookup', 'r').read()

	####### filter failed DNS lookups (not a 'true' DoT server) - TRUEDOT
	if 'Cannot make the DNS request:' in dnslookup_res:
		continue
	if ';; QUESTION SECTION:\n;google.com.' not in dnslookup_res:
		continue

	# this ip hosts a 'true' DoT server, store the necessary information for analysis
	true_dot_results[ip] = {
		'valid_chain': None, 
		'TLS_version': None 
	}

	###### validate certificate chain - CERT
	# self-signed signature => no certificate chain
	if tls_results[ip]['server_certificates']['certificate']['parsed']['signature']['self_signed']:
		true_dot_results[ip]['valid_chain'] = False
		continue

	# it is impossible to validate a certificate chain without a chain
	if 'chain' not in tls_results[ip]['server_certificates'] or len(tls_results[ip]['server_certificates']['chain']) == 0:
		true_dot_results[ip]['valid_chain'] = False
		continue

	## validate whether certificate has been issued by chain cert, etc..
	# certSequence = [rootcert, intermediatecert...?, servercert]
	certSequence = tls_results[ip]['server_certificates']['chain'].copy()
	certSequence.reverse()
	certSequence.append(tls_results[ip]['server_certificates']['certificate'])
	# pprint.pprint(certSequence)
	
	parent_cert = certSequence[0]
	for i in range(1, len(certSequence)):
		child_cert = certSequence[i]
		if not child_cert['parsed']['issuer_dn'] == parent_cert['parsed']['subject_dn']:
			true_dot_results[ip]['valid_chain'] = False
			break

		parent_cert = child_cert

	# all certificate chain checks passed, so we assume that the certificate is valid.
	if true_dot_results[ip]['valid_chain'] != False:
		true_dot_results[ip]['valid_chain'] = True
	


	


###################################### find out TLS version used by true DoT servers from goscanner.hosts.csv - TLSVER
# read result of goscanner scan
# CSV format: host,rtt,port,server_name,synStart,synEnd,scanEnd,protocol,cipher,resultString,verify_err_no,verify_code,server_version,depth,depth_verbose,error_data
goscanner_hosts = csv.reader(open('goscanner.hosts.csv', 'r'))
tlsvers = set() # used in output section, stores all different TLS versions encountered
for data in goscanner_hosts:
	# check if ip hosts true dot server, otherwise the results are not important
	ip = data[0]
	if ip not in true_dot_results:
		continue

	# record TLS version used in results
	true_dot_results[ip]['TLS_version'] = data[7]

	# goscanner scan failed to establish a proper connection, use the version from zgrab results instead (does not support TLSv1.3)
	if data[7] == 'TLS_NULL_WITH_NULL_NULL':
		true_dot_results[ip]['TLS_version'] = tls_results[ip]['server_hello']['version']['name']
		
	# store different TLS versions used in set, so that we can easily display them in the output section.
	if true_dot_results[ip]['TLS_version'] not in tlsvers:
		tlsvers.add(true_dot_results[ip]['TLS_version'])



########################################################### Output final results to stdout
print("Detailed information about true DoT server results:")
pprint.pprint(true_dot_results)

# TRUEDOT
print("Number of public DoT servers found (TRUEDOT): " + str(len(true_dot_results)))

# TLSVER
for tlsver in sorted(tlsvers, reverse=True):
	n = 0
	for ip in true_dot_results:
		if true_dot_results[ip]['TLS_version'] == tlsver:
			n += 1
	print("Percentage of public DoT servers using " + tlsver + " (TLSVER): " + str(round(n/len(true_dot_results)*100, 1)) + "%")

# CERT
n = 0
for ip in true_dot_results:
	if true_dot_results[ip]['valid_chain'] == True:
		n += 1
print("Percentage of public DoT servers with valid certificate chains (CERT): " + str(round(n/len(true_dot_results)*100, 1)) + "%")