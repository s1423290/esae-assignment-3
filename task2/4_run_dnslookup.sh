#!/bin/bash

export PATH=/home/tls13/tls13/go/bin/:$PATH

export LC_ALL=C

INFILE=$1
INFILE_BASE=`basename $INFILE .json.gz`
OUTPUT_DIR=`dirname $INFILE`

mkdir -p $OUTPUT_DIR/dnslookup-results

# Adjust as needed
DNSLOOKUP=/srv/mxtls/esae-week5-assignment-experiment/dot-scan/dnslookup

# get input
# zcat zgrab.json.gz | jq -r 'select(.data.tls.status=="success") | .ip' | gzip > ${OUTPUT_DIR}/$INFILE_BASE.ip_success.csv.gz

# run dnslookup
zcat ${OUTPUT_DIR}/$INFILE_BASE.ip_success.csv.gz | xargs -I {} -n 1 -P 10 ./dnslookup.sh {} ${OUTPUT_DIR}/dnslookup-results
