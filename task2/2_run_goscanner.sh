#!/bin/bash

export PATH=/home/tls13/tls13/go/bin/:$PATH

export LC_ALL=C

INFILE=$1
OUTPUT_DIR=`dirname $INFILE`
META_DIR=${OUTPUT_DIR}/meta
USER=$2

GOSCANNER_LOG_FILE=${META_DIR}/goscanner.log
GOSCANNER_OUTPUT_DIR=${OUTPUT_DIR}/
TCPDUMP_OUTPUT_FILE=${OUTPUT_DIR}/goscanner.tcpdump.pcap
TCPDUMP_LOGFILE=${META_DIR}/goscanner.tcpdump.log

mkdir -p $META_DIR

# Increase file limit
ulimit -Hn 1024000
ulimit -Sn 65535

# TCP: too many orphaned sockets
echo 16384 > /proc/sys/net/ipv4/tcp_max_orphans

# Adjust as needed
GOSCANNER=/home/mxtls/workspace-go-old/src/github.com/tumi8/goscanner/goscanner
GOSCANNER_CONF_FILE=/srv/mxtls/esae-week5-assignment-experiment/dot-scan/goscanner.conf
TCPDUMP=/usr/sbin/tcpdump
TCPDUMP_IFACE=eno1

# Create meta file
echo $DATE >> ${META_DIR}/goscanner.meta

# copy configuration
CONF_BASENAME=`basename $GOSCANNER_CONF_FILE`
cp $GOSCANNER_CONF_FILE $META_DIR/$CONF_BASENAME

# shuffle input
zcat $INFILE | shuf > $INFILE.shuf

# run tcpdump
$TCPDUMP -n -i $TCPDUMP_IFACE -w $TCPDUMP_OUTPUT_FILE "tcp port 853" 2> $TCPDUMP_LOGFILE &
TCPDUMPPID=$!
sleep 5

# run goscanner
$GOSCANNER -C $GOSCANNER_CONF_FILE -i $INFILE.shuf -o $GOSCANNER_OUTPUT_DIR -l $GOSCANNER_LOG_FILE
sleep 5

# stop tcpdump
/bin/kill -INT $TCPDUMPPID
sleep 5

# rename output files
mv $OUTPUT_DIR/hosts.csv $OUTPUT_DIR/goscanner.hosts.csv
mv $OUTPUT_DIR/certs.csv $OUTPUT_DIR/goscanner.certs.csv
mv $OUTPUT_DIR/cert_host_rel.csv $OUTPUT_DIR/goscanner.cert_host_rel.csv

chown -R $USER:$USER $OUTPUT_DIR
